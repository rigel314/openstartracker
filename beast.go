package openstartracker

import (
	"fmt"
	"math"
	"sort"
)

type Match_result struct {
	cfg *Calibration

	Match *Constellation_pair

	R11, R12, R13 float32
	R21, R22, R23 float32
	R31, R32, R33 float32

	img_mask *Star_fov
	mapstar  []int
	db_const *Constellation
	db, img  *Constellation_db
}

func NewMatch_result(cfg *Calibration, db, img *Constellation_db, img_mask *Star_fov) *Match_result {
	mr := &Match_result{
		cfg:      cfg,
		db:       db,
		img:      img,
		img_mask: img_mask,
		mapstar:  make([]int, img.Stars.sz),
		Match: &Constellation_pair{
			Totalscore: -math.MaxFloat32,
		},
	}

	return mr
}

func (mr *Match_result) Size() int { return len(mr.mapstar) }

func (mr *Match_result) Init(db_const, img_const *Constellation) {
	mr.db_const = db_const

	mr.Match.Img_s1 = img_const.S1
	mr.Match.Img_s2 = img_const.S2
	mr.Match.Db_s1 = db_const.S1
	mr.Match.Db_s2 = db_const.S2
}

func (mr *Match_result) Copy_over(c *Match_result) {
	c.Match = mr.Match
	c.db_const = mr.db_const

	c.R11, c.R12, c.R13 = mr.R11, mr.R12, mr.R13
	c.R21, c.R22, c.R23 = mr.R21, mr.R22, mr.R23
	c.R31, c.R32, c.R33 = mr.R31, mr.R32, mr.R33

	if len(c.mapstar) < len(mr.mapstar) {
		c.mapstar = make([]int, len(mr.mapstar))
	}
	copy(c.mapstar, mr.mapstar)
}

func (mr *Match_result) Related(m *Constellation_pair) bool {
	if mr.Match.Totalscore == -math.MaxFloat32 || m.Totalscore == -math.MaxFloat32 {
		return false
	}
	return mr.mapstar[m.Img_s1] == m.Db_s1 && mr.mapstar[m.Img_s2] == m.Db_s2
}

func (mr *Match_result) Search() {
	if mr.db.Results.Is_kdsorted() {
		mr.db.Results.Kdsearch(mr.R11, mr.R21, mr.R31, mr.cfg.MaxFov/2, float32(mr.cfg.ThreshFactor)*mr.cfg.ImageVariance)
	}
}

func (mr *Match_result) Clear_search() {
	if mr.db.Results.Is_kdsorted() {
		mr.db.Results.Clear_kdresults()
	}
}

func (mr *Match_result) Compute_score() {
	mr.Match.Totalscore = float32(math.Log(1/float64(mr.cfg.ImageWidth)*float64(mr.cfg.ImageHeight))) * 2 * float32(len(mr.mapstar))
	scores := make([]float32, len(mr.mapstar))
	for i := 0; i < len(mr.mapstar); i++ {
		mr.mapstar[i] = -1
		// scores[i] = 0
	}
	for i := 0; i < len(mr.db.Results.Results()); i++ {
		s := mr.db.Results.Map[mr.db.Results.Results()[i]]
		o := s.Star_idx
		x := s.X*mr.R11 + s.Y*mr.R21 + s.Z*mr.R31
		y := s.X*mr.R12 + s.Y*mr.R22 + s.Z*mr.R32
		z := s.X*mr.R13 + s.Y*mr.R23 + s.Z*mr.R33
		px := y / (x * mr.cfg.PixXTangent)
		py := z / (x * mr.cfg.PixYTangent)

		n := mr.img_mask.Get_id(px, py)
		if n >= 0 {
			score := mr.img_mask.Get_score(n, px, py)
			if score > scores[n] {
				mr.mapstar[n] = o
				scores[n] = score
			}
		}
	}
	for n := 0; n < len(mr.mapstar); n++ {
		mr.Match.Totalscore += scores[n]
	}
}

func (mr *Match_result) From_match() *Star_db {
	if mr.Match.Totalscore == -math.MaxFloat32 {
		return nil
	}

	s := mr.img.Stars.Copy()
	s.Max_variance = mr.db.Stars.Max_variance
	for n := 0; n < len(mr.mapstar); n++ {
		if mr.mapstar[n] != -1 {
			*s.Get_star(mr.img.Stars.Get_star(n).Star_idx) = *mr.db.Stars.Get_star(mr.mapstar[n])
		} else {
			s.Get_star(mr.img.Stars.Get_star(n).Star_idx).Id = -1
		}
	}
	return s
}

func (mr *Match_result) Weighted_triad() {
	db_s1 := mr.db.Stars.Get_star(mr.Match.Db_s1)
	db_s2 := mr.db.Stars.Get_star(mr.Match.Db_s2)
	img_s1 := mr.db.Stars.Get_star(mr.Match.Img_s1)
	img_s2 := mr.db.Stars.Get_star(mr.Match.Img_s2)

	wa1, wa2, wa3 := db_s1.X, db_s1.Y, db_s1.Z
	wb1, wb2, wb3 := db_s2.X, db_s2.Y, db_s2.Z
	va1, va2, va3 := img_s1.X, img_s1.Y, img_s1.Z
	vb1, vb2, vb3 := img_s2.X, img_s2.Y, img_s2.Z

	wc1 := wa2*wb3 - wa3*wb2
	wc2 := wa3*wb1 - wa1*wb3
	wc3 := wa1*wb2 - wa2*wb1
	wcnorm := float32(math.Sqrt(float64(wc1)*float64(wc1) + float64(wc2)*float64(wc2) + float64(wc3)*float64(wc3)))
	wc1 /= wcnorm
	wc2 /= wcnorm
	wc3 /= wcnorm

	vc1 := va2*vb3 - va3*vb2
	vc2 := va3*vb1 - va1*vb3
	vc3 := va1*vb2 - va2*vb1
	vcnorm := float32(math.Sqrt(float64(vc1)*float64(vc1) + float64(vc2)*float64(vc2) + float64(vc3)*float64(vc3)))
	vc1 /= vcnorm
	vc2 /= vcnorm
	vc3 /= vcnorm

	vaXvc1 := va2*vc3 - va3*vc2
	vaXvc2 := va3*vc1 - va1*vc3
	vaXvc3 := va1*vc2 - va2*vc1

	waXwc1 := wa2*wc3 - wa3*wc2
	waXwc2 := wa3*wc1 - wa1*wc3
	waXwc3 := wa1*wc2 - wa2*wc1

	A11 := va1*wa1 + vaXvc1*waXwc1 + vc1*wc1
	A21 := va2*wa1 + vaXvc2*waXwc1 + vc2*wc1
	A31 := va3*wa1 + vaXvc3*waXwc1 + vc3*wc1
	A32 := va3*wa2 + vaXvc3*waXwc2 + vc3*wc2
	A33 := va3*wa3 + vaXvc3*waXwc3 + vc3*wc3

	wc1 = -wc1
	wc2 = -wc2
	wc3 = -wc3

	vc1 = -vc1
	vc2 = -vc2
	vc3 = -vc3
	vbXvc1 := vb2*vc3 - vb3*vc2
	vbXvc2 := vb3*vc1 - vb1*vc3
	vbXvc3 := vb1*vc2 - vb2*vc1

	wbXwc1 := wb2*wc3 - wb3*wc2
	wbXwc2 := wb3*wc1 - wb1*wc3
	wbXwc3 := wb1*wc2 - wb2*wc1

	B11 := vb1*wb1 + vbXvc1*wbXwc1 + vc1*wc1
	B21 := vb2*wb1 + vbXvc2*wbXwc1 + vc2*wc1
	B31 := vb3*wb1 + vbXvc3*wbXwc1 + vc3*wc1
	B32 := vb3*wb2 + vbXvc3*wbXwc2 + vc3*wc2
	B33 := vb3*wb3 + vbXvc3*wbXwc3 + vc3*wc3

	weightA := 1 / (db_s1.Sigma_sq + img_s1.Sigma_sq)
	weightB := 1 / (db_s2.Sigma_sq + img_s2.Sigma_sq)

	sumAB := weightA + weightB
	weightA /= sumAB
	weightB /= sumAB

	cz := weightA*A11 + weightB*B11
	sz := weightA*A21 + weightB*B21
	mz := float32(math.Sqrt(float64(cz)*float64(cz) + float64(sz)*float64(sz)))
	cz = cz / mz
	sz = sz / mz

	cy := weightA*float32(math.Sqrt(float64(A32)*float64(A32)+float64(A33)*float64(A33))) + weightB*float32(math.Sqrt(float64(B32)*float64(B32)+float64(B33)*float64(B33)))
	sy := -weightA*A31 - weightB*B31
	my := float32(math.Sqrt(float64(cy)*float64(cy) + float64(sy)*float64(sy)))
	cy = cy / my
	sy = sy / my

	cx := weightA*A33 + weightB*B33
	sx := weightA*A32 + weightB*B32
	mx := float32(math.Sqrt(float64(cx)*float64(cx) + float64(sx)*float64(sx)))
	cx = cx / mx
	sx = sx / mx

	mr.R11 = cy * cz
	mr.R21 = cz*sx*sy - cx*sz
	mr.R31 = sx*sz + cx*cz*sy

	mr.R12 = cy * sz
	mr.R22 = cx*cz + sx*sy*sz
	mr.R32 = cx*sy*sz - cz*sx

	mr.R13 = -sy
	mr.R23 = cy * sx
	mr.R33 = cx * cy
}

func (mr *Match_result) String() string {
	return fmt.Sprintf(
		"match_result: {{%f,%f,%f},{%f,%f,%f},{%f,%f,%f}}, map_size: %d",
		mr.R11, mr.R12, mr.R13,
		mr.R21, mr.R22, mr.R23,
		mr.R31, mr.R32, mr.R33,
		len(mr.mapstar),
	)
}

type Db_match struct {
	cfg      *Calibration
	c_pairs  []*Constellation_pair
	img_mask *Star_fov

	P_match float32
	Winner  *Match_result
}

func NewDb_match(cfg *Calibration, db, img *Constellation_db) *Db_match {
	dbret := &Db_match{
		cfg: cfg,
	}

	if db.Stars.sz < 3 || img.Stars.sz < 3 {
		return dbret
	}

	dbret.img_mask = NewStar_fov(dbret.cfg, img.Stars, db.Stars.Max_variance)

	m := NewMatch_result(dbret.cfg, db, img, dbret.img_mask)
	dbret.Winner = NewMatch_result(dbret.cfg, db, img, dbret.img_mask)
	for n := 0; n < len(img.Map); n++ {
		lb := *img.Map[n]
		ub := *img.Map[n]
		sub := dbret.cfg.PosErrSigma*dbret.cfg.PixelScale*float32(math.Sqrt(float64(img.Stars.Get_star(lb.S1).Sigma_sq)+float64(img.Stars.Get_star(lb.S2).Sigma_sq))) + 2*db.Stars.Max_variance
		lb.P -= sub
		add := dbret.cfg.PosErrSigma*dbret.cfg.PixelScale*float32(math.Sqrt(float64(img.Stars.Get_star(ub.S1).Sigma_sq)+float64(img.Stars.Get_star(ub.S2).Sigma_sq))) + 2*db.Stars.Max_variance
		ub.P += add
		ln := sort.Search(len(db.Map), func(i int) bool {
			return constellation_lt_p(&lb, db.Map[i])
		})
		if ln == len(db.Map) {
			continue
		}
		lower := db.Map[ln]
		un := sort.Search(len(db.Map), func(i int) bool {
			return constellation_lt_p(&ub, db.Map[i])
		})
		upper := db.Map[un-1]
		for o := lower.Idx; o <= upper.Idx; o++ {
			m.Init(db.Map[o], img.Map[n])
			m.Weighted_triad()
			m.Search()
			dbret.add_score(m)

			m.Match.Flip()
			m.Weighted_triad()
			dbret.add_score(m)
			m.Clear_search()
		}
	}

	if dbret.Winner.Match.Totalscore != -math.MaxFloat32 {
		dbret.P_match = 1

		for idx := 0; idx < len(dbret.c_pairs); idx++ {
			if !dbret.Winner.Related(dbret.c_pairs[idx]) {
				dbret.P_match += float32(math.Exp(float64(dbret.c_pairs[idx].Totalscore) - float64(dbret.Winner.Match.Totalscore)))
			}
		}

		dbret.P_match = 1 / dbret.P_match
	}

	return dbret
}

func (db *Db_match) add_score(m *Match_result) {
	m.Compute_score()
	if m.Match.Totalscore > db.Winner.Match.Totalscore {
		if db.Winner.Match.Totalscore != -math.MaxFloat32 {
			db.c_pairs = append(db.c_pairs, db.Winner.Match) // TODO: figure out if this should copy
		}
		m.Copy_over(db.Winner)
	} else {
		db.c_pairs = append(db.c_pairs, m.Match)
	}
}
