package openstartracker

import (
	"bytes"
	_ "embed"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"math"
	"sort"

	"github.com/spenczar/fpc"
	"github.com/wangjohn/quickselect"
)

type Star struct {
	X, Y, Z, Flux float32
	Id            int
	Px, Py        float32
	Unreliable    int
	Star_idx      int
	Sigma_sq      float32
	Hash_val      Hash
}

func NewCatalogStar(cfg *Calibration, x, y, z, flux float32, id int) *Star {
	return &Star{
		X:          x,
		Y:          y,
		Z:          z,
		Flux:       flux,
		Id:         id,
		Px:         y / (x * cfg.PixXTangent),
		Py:         z / (x * cfg.PixXTangent),
		Unreliable: 0,
		Star_idx:   -1,
		Sigma_sq:   cfg.PosVariance,
		Hash_val:   Hash3(x, y, z),
	}
}

func NewImageStar(cfg *Calibration, px, py, flux float32, id int) *Star {
	j := float64(cfg.PixXTangent * px)
	k := float64(cfg.PixYTangent * py)
	x := 1 / math.Sqrt(j*j+k*k+1)
	y := j * x
	z := k * x
	return &Star{
		Px:         px,
		Py:         py,
		Flux:       flux,
		Id:         id,
		X:          float32(x),
		Y:          float32(y),
		Z:          float32(z),
		Unreliable: 0,
		Star_idx:   -1,
		Sigma_sq:   cfg.ImageVariance / flux,
		Hash_val:   Hash3(float32(x), float32(y), float32(z)),
	}
}

func (s *Star) Equal(other Star) bool { return s.Hash_val == other.Hash_val }

func (s *Star) Dist_arcsec(other Star) float32 {
	a := s.X*other.Y - other.X*s.Y
	b := s.X*other.Z - other.X*s.Z
	c := s.Y*other.Z - other.Y*s.Z
	return (3600 * 180 / math.Pi) * float32(math.Asin(math.Sqrt(float64(a*a+b*b+c*c))))
}

func (s *Star) String() string {
	return fmt.Sprintf(
		"star: x:%f, y:%f, z:%f, flux:%f, star_idx:%d, id:%d, unreliable:%d, sigma_sq:%f, px:%f, py:%f",
		s.X, s.Y, s.Z, s.Flux,
		s.Star_idx, s.Id, s.Unreliable,
		s.Sigma_sq, s.Px, s.Py,
	)
}

func star_gt_x(s1, s2 *Star) bool    { return s1.X > s2.X }
func star_gt_y(s1, s2 *Star) bool    { return s1.Y > s2.Y }
func star_gt_z(s1, s2 *Star) bool    { return s1.Z > s2.Z }
func star_gt_flux(s1, s2 *Star) bool { return s1.Flux > s2.Flux }
func star_lt_x(s1, s2 *Star) bool    { return s1.X < s2.X }
func star_lt_y(s1, s2 *Star) bool    { return s1.Y < s2.Y }
func star_lt_z(s1, s2 *Star) bool    { return s1.Z < s2.Z }
func star_lt_flux(s1, s2 *Star) bool { return s1.Flux < s2.Flux }

type Star_db struct {
	hash_map map[Hash]*Star
	// TODO: hash_set ?  std::set -> golang ?
	star_idx_vector []Hash
	flux_map        map[float32][]Hash
	sz              uint64
	cfg             *Calibration

	Max_variance float32
}

func NewStar_db(cfg *Calibration) *Star_db {
	return &Star_db{
		hash_map: make(map[Hash]*Star),
		flux_map: make(map[float32][]Hash),
		cfg:      cfg,
	}
}

func (db *Star_db) Size() uint64 { return db.sz }

func (db *Star_db) Append(s *Star) {
	if !db.HasStar(s) {
		if db.Max_variance < s.Sigma_sq {
			db.Max_variance = s.Sigma_sq
		}
		scopy := *s
		scopy.Star_idx = int(db.sz)
		db.hash_map[scopy.Hash_val] = &scopy
		// db.hash_set append new_s.Hash_val // TODO: make this real
		db.flux_map[scopy.Flux] = append(db.flux_map[scopy.Flux], scopy.Hash_val)
		db.star_idx_vector = append(db.star_idx_vector, scopy.Hash_val)
		db.sz++
	}
}

func (db *Star_db) Without(s *Star_db) *Star_db {
	ret := NewStar_db(db.cfg)
	for _, v := range db.hash_map {
		if !s.HasStar(v) {
			ret.Append(v)
		}
	}
	return ret
}

func (db *Star_db) Intersection(s *Star_db) *Star_db {
	ret := NewStar_db(db.cfg)
	for _, v := range db.hash_map {
		if s.HasStar(v) {
			ret.Append(v)
		}
	}
	return ret
}

func (db *Star_db) Get_star_by_hash(h Hash) *Star {
	s, ok := db.hash_map[h]
	if ok {
		return s
	}
	return nil
}

func (db *Star_db) Get_star(idx int) *Star {
	s, ok := db.hash_map[db.star_idx_vector[idx]]
	if ok {
		return s
	}
	return nil
}

func (db *Star_db) Copy() *Star_db {
	ret := NewStar_db(db.cfg)
	for _, v := range db.hash_map {
		ret.Append(v)
	}
	return ret
}

func (db *Star_db) Copy_n_brightest(n int) *Star_db {
	ret := NewStar_db(db.cfg)
	count := 0
	for _, v := range db.flux_map {
		for _, s := range v {
			ret.Append(db.hash_map[s])
			count++
			if count >= n {
				return ret
			}
		}
	}
	return ret
}

func (db *Star_db) HasStar(s *Star) bool {
	_, ok := db.hash_map[s.Hash_val]
	return ok
}

func (db *Star_db) Count(s *Star_db) int {
	count := 0
	for _, v := range db.hash_map {
		if !s.HasStar(v) {
			count++
		}
	}

	return count
}

// func (db *Star_db) Search(hs Hash, x, y, z, r, min_flux float32) {
// 	r = r / 3600
// 	r = r * math.Pi / 180
// 	r = 2 * float32(math.Abs(math.Sin(float64(r/2))))
// 	mask := Mask3(r)
// }

//go:generate go run gitlab.com/rigel314/openstartracker/gen/catalogbin data/hip_main.dat

var (
	StarCatalogIndex     []int
	StarCatalogMagnitude []float64
	StarCatalogRA        []float64
	StarCatalogRArate    []float64
	StarCatalogDEC       []float64
	StarCatalogDECrate   []float64
	StarCatalogReliable1 []int
	StarCatalogReliable2 []int
)

var (
	//go:embed data/index.var
	starCatalogIndexFile []byte
	//go:embed data/mag.fpc
	starCatalogMagnitudeFile []byte
	//go:embed data/ra_epoch.fpc
	starCatalogRAFile []byte
	//go:embed data/ra_rate.fpc
	starCatalogRArateFile []byte
	//go:embed data/dec_epoch.fpc
	starCatalogDECFile []byte
	//go:embed data/dec_rate.fpc
	starCatalogDECrateFile []byte
	//go:embed data/rel1.var
	starCatalogReliable1File []byte
	//go:embed data/rel2.var
	starCatalogReliable2File []byte
)

func decodevarint(dest *[]int, src []byte) error {
	rd := bytes.NewReader(src)
	for {
		val, err := binary.ReadUvarint(rd)
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			return err
		}
		*dest = append(*dest, int(val))
	}
	return nil
}
func decodefpc(dest *[]float64, src []byte) error {
	rd := bytes.NewReader(src)
	frd := fpc.NewReader(rd)
	count := 0
	for {
		val, err := frd.ReadFloat()
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			return fmt.Errorf("%w, count:%d", err, count)
		}
		*dest = append(*dest, val)
		count++
	}
	return nil
}

func (db *Star_db) Load_builtin_catalog(year float32) error {
	err0 := decodevarint(&StarCatalogIndex, starCatalogIndexFile)
	err1 := decodevarint(&StarCatalogReliable1, starCatalogReliable1File)
	err2 := decodevarint(&StarCatalogReliable2, starCatalogReliable2File)
	err3 := decodefpc(&StarCatalogMagnitude, starCatalogMagnitudeFile)
	err4 := decodefpc(&StarCatalogRA, starCatalogRAFile)
	err5 := decodefpc(&StarCatalogRArate, starCatalogRArateFile)
	err6 := decodefpc(&StarCatalogDEC, starCatalogDECFile)
	err7 := decodefpc(&StarCatalogDECrate, starCatalogDECrateFile)

	if err := firstErr(err0, err1, err2, err3, err4, err5, err6, err7); err != nil {
		return err
	}

	return db.Load_catalog(year)
}

func firstErr(errs ...error) error {
	for _, v := range errs {
		if v != nil {
			return v
		}
	}
	return nil
}

func (db *Star_db) Load_catalog(year float32) error {
	if len(StarCatalogIndex) == 0 {
		return fmt.Errorf("uninitialized catalog")
	}
	if !(len(StarCatalogIndex) == len(StarCatalogReliable1) &&
		len(StarCatalogReliable1) == len(StarCatalogReliable2) &&
		len(StarCatalogReliable2) == len(StarCatalogMagnitude) &&
		len(StarCatalogMagnitude) == len(StarCatalogRA) &&
		len(StarCatalogRA) == len(StarCatalogRArate) &&
		len(StarCatalogRArate) == len(StarCatalogDEC) &&
		len(StarCatalogDEC) == len(StarCatalogDECrate)) {
		return fmt.Errorf("catalog components are not all the same length")
	}

	db.Max_variance = db.cfg.PosVariance

	yeardiff := year - 1991.25

	for i := 0; i < len(StarCatalogIndex); i++ {
		mag := float32(StarCatalogMagnitude[i])
		dec := yeardiff*float32(StarCatalogDECrate[i])/3600e3 + float32(StarCatalogDEC[i])
		cosdec := float32(math.Cos(math.Pi * float64(dec) / 180))
		ra := yeardiff*float32(StarCatalogRArate[i])/(cosdec*3600e3) + float32(StarCatalogRA[i])

		s := NewCatalogStar(
			db.cfg,
			float32(math.Cos(math.Pi*float64(ra)/180))*cosdec,
			float32(math.Sin(math.Pi*float64(ra)/180))*cosdec,
			float32(math.Sin(math.Pi*float64(dec)/180)),
			db.cfg.BaseFlux*float32(math.Pow(10, float64(-mag)/2.5)),
			StarCatalogIndex[i],
		)

		if !((StarCatalogReliable1[i] == 0 || StarCatalogReliable1[i] == 1) && StarCatalogReliable2[i] != 3) {
			s.Unreliable = 1
		}

		db.Append(s)
	}

	return nil
}

func (db *Star_db) String() string {
	return fmt.Sprintf(
		"star_db: size: %d, max_variance: %f",
		db.sz,
		db.Max_variance,
	)
}

type Star_fov struct {
	cfg             *Calibration
	mask            []int
	stars           *Star_db
	collision       []int
	db_max_variance float32
	s_px, s_py      []float32
}

func NewStar_fov(cfg *Calibration, s *Star_db, db_max_variance float32) *Star_fov {
	mask := make([]int, cfg.ImageWidth*cfg.ImageHeight)
	for i := 0; i < cfg.ImageWidth*cfg.ImageHeight; i++ {
		mask[i] = -1
	}
	fov := &Star_fov{
		cfg:             cfg,
		mask:            mask,
		stars:           s,
		db_max_variance: db_max_variance,
		s_px:            make([]float32, s.sz),
		s_py:            make([]float32, s.sz),
	}

	for id := 0; id < int(fov.stars.sz); id++ {
		sigma_sq := fov.stars.Max_variance + fov.db_max_variance
		maxdist_sq := -sigma_sq * (float32(math.Log(float64(sigma_sq))) + fov.cfg.MatchValue)
		maxdist := float32(math.Sqrt(float64(maxdist_sq)))
		fov.s_px[id] = fov.stars.Get_star(id).Px
		fov.s_py[id] = fov.stars.Get_star(id).Py

		xmin := int(fov.s_px[id] - maxdist - 1)
		xmax := int(fov.s_px[id] + maxdist + 1)
		ymin := int(fov.s_py[id] - maxdist - 1)
		ymax := int(fov.s_py[id] + maxdist + 1)

		if xmax > fov.cfg.ImageWidth/2 {
			xmax = fov.cfg.ImageWidth / 2
		}
		if xmin < -fov.cfg.ImageWidth/2 {
			xmin = -fov.cfg.ImageWidth / 2
		}
		if ymax > fov.cfg.ImageHeight/2 {
			ymax = fov.cfg.ImageHeight / 2
		}
		if ymin < -fov.cfg.ImageHeight/2 {
			ymin = -fov.cfg.ImageHeight / 2
		}

		for i := xmin; i < xmax; i++ {
			for j := ymin; j < ymax; j++ {
				score := fov.Get_score_WithSigmaDist(id, float32(i), float32(j), sigma_sq, maxdist_sq)

				x := i + fov.cfg.ImageWidth/2
				y := j + fov.cfg.ImageHeight/2

				if score > 0 {
					id2 := fov.mask[x+y*fov.cfg.ImageWidth]
					if id2 != -1 {
						fov.collision = append(fov.collision, id)
						fov.collision = append(fov.collision, id2)
						mask[x+y*fov.cfg.ImageWidth] = -len(fov.collision)
					} else {
						mask[x+y*fov.cfg.ImageWidth] = id
					}
				}
			}
		}
	}

	return fov
}

func (fov *Star_fov) resolve_id(id int, px, py float32) int {
	if id >= -1 {
		return id
	}
	id = -id

	id1 := fov.resolve_id(fov.collision[id-2], px, py)
	id2 := fov.resolve_id(fov.collision[id-1], px, py)

	if fov.Get_score(id1, px, py) > fov.Get_score(id2, px, py) {
		return id1
	}

	return id2
}

func (fov *Star_fov) Get_score(id int, px, py float32) float32 {
	sigma_sq := fov.stars.Max_variance + fov.db_max_variance
	maxdist_sq := -sigma_sq * (float32(math.Log(float64(sigma_sq))) + fov.cfg.MatchValue)

	return fov.Get_score_WithSigmaDist(id, px, py, sigma_sq, maxdist_sq)
}

func (fov *Star_fov) Get_score_WithSigmaDist(id int, px, py, sigma_sq, maxdist_sq float32) float32 {
	dx := px - fov.s_px[id]
	dy := py - fov.s_py[id]
	if dx < -.5 {
		dx += 1
	}
	if dy < -.5 {
		dy += 1
	}

	return (maxdist_sq - (dx*dx + dy*dy)) / (2 * sigma_sq)
}

func (fov *Star_fov) Get_id(px, py float32) int {
	nx := int(px + float32(fov.cfg.ImageWidth)/2)
	if nx == -1 {
		nx++
	} else if nx == fov.cfg.ImageWidth {
		nx--
	}

	ny := int(py + float32(fov.cfg.ImageHeight)/2)
	if ny == -1 {
		ny++
	} else if ny == fov.cfg.ImageHeight {
		ny--
	}

	id := -1

	if nx >= 0 && nx < fov.cfg.ImageWidth && ny >= 0 && ny < fov.cfg.ImageHeight {
		id = fov.mask[nx+ny*fov.cfg.ImageWidth]
	}

	return fov.resolve_id(id, px, py)
}

type Star_query struct {
	Map []*Star

	kdresults         []int
	cfg               *Calibration
	kdsorted          bool
	kdmask            []int8
	stars             *Star_db
	kdresults_size    int
	kdresults_maxsize int
}

func NewStar_query(cfg *Calibration, s *Star_db) *Star_query {
	q := &Star_query{
		Map:               make([]*Star, s.sz),
		kdresults:         make([]int, s.sz+1),
		cfg:               cfg,
		kdmask:            make([]int8, s.sz+1),
		stars:             s,
		kdresults_size:    int(s.sz),
		kdresults_maxsize: math.MaxInt,
	}

	for i := 0; i < int(q.stars.sz); i++ {
		q.Map[i] = q.stars.Get_star(i) // TODO: figure out if this should make a copy
		q.kdresults[i] = i
	}

	return q
}

func (q *Star_query) kdsort_x(min, max int) {
	mid := (min + max) / 2
	if min+1 < max {
		nth_element(q.Map[min:max+1], mid-min, star_lt_x)
		if mid-min > q.cfg.KDBucketSize {
			q.kdsort_y(min, mid)
		} else {
			sort.Slice(q.Map[min:mid+1], func(i int, j int) bool {
				return star_gt_flux(q.Map[min : mid+1][i], q.Map[min : mid+1][j])
			})
		}
		if max-(mid+1) > q.cfg.KDBucketSize {
			q.kdsort_y(mid+1, max)
		} else {
			sort.Slice(q.Map[mid+1:max+1], func(i int, j int) bool {
				return star_gt_flux(q.Map[mid+1 : max+1][i], q.Map[mid+1 : max+1][j])
			})
		}
	}
}

func (q *Star_query) kdsort_y(min, max int) {
	mid := (min + max) / 2
	if min+1 < max {
		nth_element(q.Map[min:max+1], mid-min, star_lt_y)
		if mid-min > q.cfg.KDBucketSize {
			q.kdsort_z(min, mid)
		} else {
			sort.Slice(q.Map[min:mid+1], func(i int, j int) bool {
				return star_gt_flux(q.Map[min : mid+1][i], q.Map[min : mid+1][j])
			})
		}
		if max-(mid+1) > q.cfg.KDBucketSize {
			q.kdsort_z(mid+1, max)
		} else {
			sort.Slice(q.Map[mid+1:max+1], func(i int, j int) bool {
				return star_gt_flux(q.Map[mid+1 : max+1][i], q.Map[mid+1 : max+1][j])
			})
		}
	}
}

func (q *Star_query) kdsort_z(min, max int) {
	mid := (min + max) / 2
	if min+1 < max {
		nth_element(q.Map[min:max+1], mid-min, star_lt_z)
		if mid-min > q.cfg.KDBucketSize {
			q.kdsort_x(min, mid)
		} else {
			sort.Slice(q.Map[min:mid+1], func(i int, j int) bool {
				return star_gt_flux(q.Map[min : mid+1][i], q.Map[min : mid+1][j])
			})
		}
		if max-(mid+1) > q.cfg.KDBucketSize {
			q.kdsort_x(mid+1, max)
		} else {
			sort.Slice(q.Map[mid+1:max+1], func(i int, j int) bool {
				return star_gt_flux(q.Map[mid+1 : max+1][i], q.Map[mid+1 : max+1][j])
			})
		}
	}
}

func nth_element(stars []*Star, n int, fn func(s1, s2 *Star) bool) {
	s := &sel{
		data:   stars,
		length: len(stars),
		lessfn: fn,
	}
	quickselect.QuickSelect(s, n)
}

type sel struct {
	data   []*Star
	length int
	lessfn func(s1, s2 *Star) bool
}

func (s *sel) Len() int {
	return s.length
}

func (s *sel) Less(i, j int) bool {
	return s.lessfn(s.data[i], s.data[j])
}

func (s *sel) Swap(i, j int) {
	s.data[i], s.data[j] = s.data[j], s.data[i]
}

func (q *Star_query) Is_kdsorted() bool { return q.kdsorted }

func (q *Star_query) Kdsort() {
	if !q.kdsorted {
		q.kdsort_x(0, len(q.Map)-1)
		q.kdsorted = true
	}
}

func (q *Star_query) Sort() {
	sort.Slice(q.Map, func(i, j int) bool {
		return star_gt_flux(q.Map[i], q.Map[j])
	})
	q.kdsorted = false
}

func (q *Star_query) Results() []int {
	return q.kdresults[:q.kdresults_size]
}

func (q *Star_query) Get_kdmask(i int) int8 { return q.kdmask[i] }

func (q *Star_query) Reset_kdmask() {
	for i := 0; i < len(q.kdmask); i++ {
		q.kdmask[i] = 0
	}
}

func (q *Star_query) Clear_kdresults() {
	for q.kdresults_size > 0 {
		q.kdresults_size--
		q.kdmask[q.kdresults[q.kdresults_size]] = 0
	}
}

func (q *Star_query) Kdcheck(idx int, x, y, z, r, min_flux float32) {
	x -= q.Map[idx].X
	y -= q.Map[idx].Y
	z -= q.Map[idx].Z

	if x-r <= 0 && 0 <= x+r &&
		y-r <= 0 && 0 <= y+r &&
		z-r <= 0 && 0 <= z+r &&
		min_flux <= q.Map[idx].Flux &&
		q.kdmask[idx] == 0 &&
		x*x+y*y+z*z <= r*r {

		q.kdmask[idx] = 1
		n := q.kdresults_size
		q.kdresults_size++

		sm_flux := q.Map[idx].Flux
		for ; n > 0 && sm_flux > q.Map[q.kdresults[n-1]].Flux; n-- {
			q.kdresults[n] = q.kdresults[n-1]
		}
		q.kdresults[n] = idx
		if q.kdresults_size > q.kdresults_maxsize {
			q.kdresults_size = q.kdresults_maxsize
			q.kdmask[q.kdresults[q.kdresults_size]] = 0
		}
	}
}

func (q *Star_query) KdsearchWithBounds(x, y, z, r, min_flux float32, min, max, dim int) {
	q.Kdsort()
	r_deg := r / 3600
	r_rad := r_deg * math.Pi / 180
	if dim == 0 {
		q.kdsearch_x(x, y, z, float32(2*math.Abs(math.Sin(float64(r_rad)/2))), min_flux, min, max)
	} else if dim == 1 {
		q.kdsearch_y(x, y, z, float32(2*math.Abs(math.Sin(float64(r_rad)/2))), min_flux, min, max)
	} else if dim == 2 {
		q.kdsearch_z(x, y, z, float32(2*math.Abs(math.Sin(float64(r_rad)/2))), min_flux, min, max)
	}
}

func (q *Star_query) Kdsearch(x, y, z, r, min_flux float32) {
	q.KdsearchWithBounds(x, y, z, r, min_flux, 0, int(q.stars.sz), 0)
}

func (q *Star_query) kdsearch_x(x, y, z, r, min_flux float32, min, max int) {
	mid := (min + max) / 2
	if min < mid && x-r <= q.Map[mid].X {
		if mid-min > q.cfg.KDBucketSize {
			q.kdsearch_y(x, y, z, r, min_flux, min, mid)
		} else {
			for i := min; i < mid && min_flux <= q.Map[i].Flux; i++ {
				q.Kdcheck(i, x, y, z, r, min_flux)
			}
		}
	}
	if mid < max {
		q.Kdcheck(mid, x, y, z, r, min_flux)
	}
	if q.kdresults_size == q.kdresults_maxsize {
		min_flux = q.Map[q.kdresults[q.kdresults_size-1]].Flux
	}
	if mid+1 < max && q.Map[mid].X <= x+r {
		if max-(mid+1) > q.cfg.KDBucketSize {
			q.kdsearch_y(x, y, z, r, min_flux, mid+1, max)
		} else {
			for i := mid + 1; i < max && min_flux <= q.Map[i].Flux; i++ {
				q.Kdcheck(i, x, y, z, r, min_flux)
			}
		}
	}
}

func (q *Star_query) kdsearch_y(x, y, z, r, min_flux float32, min, max int) {
	mid := (min + max) / 2
	if min < mid && y-r <= q.Map[mid].Y {
		if mid-min > q.cfg.KDBucketSize {
			q.kdsearch_z(x, y, z, r, min_flux, min, mid)
		} else {
			for i := min; i < mid && min_flux <= q.Map[i].Flux; i++ {
				q.Kdcheck(i, x, y, z, r, min_flux)
			}
		}
	}
	if mid < max {
		q.Kdcheck(mid, x, y, z, r, min_flux)
	}
	if q.kdresults_size == q.kdresults_maxsize {
		min_flux = q.Map[q.kdresults[q.kdresults_size-1]].Flux
	}
	if mid+1 < max && q.Map[mid].Y <= y+r {
		if max-(mid+1) > q.cfg.KDBucketSize {
			q.kdsearch_z(x, y, z, r, min_flux, mid+1, max)
		} else {
			for i := mid + 1; i < max && min_flux <= q.Map[i].Flux; i++ {
				q.Kdcheck(i, x, y, z, r, min_flux)
			}
		}
	}
}

func (q *Star_query) kdsearch_z(x, y, z, r, min_flux float32, min, max int) {
	mid := (min + max) / 2
	if min < mid && z-r <= q.Map[mid].Z {
		if mid-min > q.cfg.KDBucketSize {
			q.kdsearch_x(x, y, z, r, min_flux, min, mid)
		} else {
			for i := min; i < mid && min_flux <= q.Map[i].Flux; i++ {
				q.Kdcheck(i, x, y, z, r, min_flux)
			}
		}
	}
	if mid < max {
		q.Kdcheck(mid, x, y, z, r, min_flux)
	}
	if q.kdresults_size == q.kdresults_maxsize {
		min_flux = q.Map[q.kdresults[q.kdresults_size-1]].Flux
	}
	if mid+1 < max && q.Map[mid].Z <= z+r {
		if max-(mid+1) > q.cfg.KDBucketSize {
			q.kdsearch_x(x, y, z, r, min_flux, mid+1, max)
		} else {
			for i := mid + 1; i < max && min_flux <= q.Map[i].Flux; i++ {
				q.Kdcheck(i, x, y, z, r, min_flux)
			}
		}
	}
}

func (q *Star_query) Kdmask_filter_catalog() {
	for i := 0; i < int(q.stars.sz); i++ {
		lastmask := q.kdmask[i]
		q.Kdsearch(q.Map[i].X, q.Map[i].Y, q.Map[i].Z, q.cfg.DoubleStarPx*q.cfg.PixelScale, float32(q.cfg.ThreshFactor)*q.cfg.ImageVariance)

		if q.kdresults_size > 1 || lastmask != 0 || q.Map[i].Flux < float32(q.cfg.ThreshFactor)*q.cfg.ImageVariance {
			q.kdmask[i] = 1
			q.kdresults_size = 0
		} else {
			q.Clear_kdresults()
		}
	}
}

func (q *Star_query) Kdmask_uniform_density(min_stars_per_fov int) {
	uniform_set := make(map[int]struct{})
	kdresults_maxsize_old := q.kdresults_maxsize
	q.kdresults_maxsize = min_stars_per_fov
	for i := 0; i < int(q.stars.sz); i++ {
		if q.kdmask[i] == 0 {
			q.Kdsearch(q.Map[i].X, q.Map[i].Y, q.Map[i].Z, q.cfg.MinFOV/2, float32(q.cfg.ThreshFactor)*q.cfg.ImageVariance)
			for _, v := range q.kdresults[:q.kdresults_size] {
				uniform_set[v] = struct{}{}
			}
			q.Clear_kdresults()
		}
	}
	for i := 0; i < int(q.stars.sz); i++ {
		q.kdmask[i] = 1
	}
	for k := range uniform_set {
		q.kdmask[k] = 0
	}
	q.kdresults_maxsize = kdresults_maxsize_old
}

func (q *Star_query) From_kdmask() *Star_db {
	rd := NewStar_db(q.cfg)
	rd.Max_variance = q.stars.Max_variance
	for i := 0; i < int(q.stars.sz); i++ {
		if q.kdmask[i] == 0 {
			rd.Append(q.Map[i]) // TODO: figure out if this should be a copy
		}
	}
	return rd
}

func (q *Star_query) From_kdresults() *Star_db {
	rd := NewStar_db(q.cfg)
	rd.Max_variance = q.stars.Max_variance
	for i := 0; i < q.kdresults_size; i++ {
		if q.kdmask[i] == 0 {
			rd.Append(q.Map[q.kdresults[i]]) // TODO: figure out if this should be a copy
		}
	}
	return rd
}

func (q *Star_query) String() string {
	return fmt.Sprintf(
		"star_query: kdsorted: %v, kdresults_size: %d, kdresults_maxsize: %d",
		q.kdsorted,
		q.kdresults_size,
		q.kdresults_maxsize,
	)
}
