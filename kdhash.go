package openstartracker

import "math"

type Hash uint64

const (
	diam2  = 0xffffffff
	r_IN2  = diam2 / 2
	r_OUT2 = r_IN2 + 1
)

func interleave2(x int64) uint64 {
	if x > diam2 {
		x = diam2
	}

	if x < 0 {
		x = 0
	}

	x = (x | x<<16) & 0xffff0000ffff
	x = (x | x<<8) & 0xff00ff00ff00ff
	x = (x | x<<4) & 0xf0f0f0f0f0f0f0f
	x = (x | x<<2) & 0x3333333333333333
	x = (x | x<<1) & 0x5555555555555555

	return uint64(x)
}

func Hash2(x0, x1 float32) Hash {
	h0 := int64(r_OUT2 * (x0 + 1))
	h1 := int64(r_OUT2 * (x1 + 1))
	return Hash(interleave2(h1)<<1 | interleave2(h0))
}

func Bin_size2(r float32) (sz uint8) {
	h := int64(r_OUT2 * r)
	if h > r_IN2 {
		h = r_IN2
	}
	if h < 0 {
		h = 0
	}

	for h != 0 {
		sz++
		h >>= 1
	}
	return sz
}

func Mask2(r float32) Hash {
	var mask Hash = math.MaxUint64
	return mask << 2 * Hash(Bin_size2(r))
}

func MaskRange2(r0, r1 float32) Hash {
	h0 := int64(r_OUT2 >> Bin_size2(r0))
	h1 := int64(r_OUT2 >> Bin_size2(r1))
	return Hash(interleave2(h1)<<1 | interleave2(h0))
}

const (
	diam3  = 0x1fffff
	r_IN3  = diam3 / 2
	r_OUT3 = r_IN3 + 1
)

func interleave3(x int64) uint64 {
	if x > diam3 {
		x = diam3
	}

	if x < 0 {
		x = 0
	}

	x = (x | x<<32) & 0x001f00000000ffff
	x = (x | x<<16) & 0x001f0000ff0000ff
	x = (x | x<<8) & 0x100f00f00f00f00f
	x = (x | x<<4) & 0x10c30c30c30c30c3
	x = (x | x<<2) & 0x1249249249249249

	return uint64(x)
}

func Hash3(x0, x1, x2 float32) Hash {
	h0 := int64(r_OUT3 * (x0 + 1))
	h1 := int64(r_OUT3 * (x1 + 1))
	h2 := int64(r_OUT3 * (x2 + 1))
	return Hash(interleave3(h2)<<2 | interleave3(h1)<<1 | interleave3(h0))
}

func Bin_size3(r float32) (sz uint8) {
	h := int64(r_OUT3 * r)
	if h > r_IN3 {
		h = r_IN3
	}
	if h < 0 {
		h = 0
	}

	for h != 0 {
		sz++
		h >>= 1
	}
	return sz
}

func Mask3(r float32) Hash {
	var mask Hash = math.MaxUint64
	return mask << 3 * Hash(Bin_size3(r))
}

func MaskRange3(r0, r1, r2 float32) Hash {
	h0 := int64(r_OUT3 >> Bin_size3(r0))
	h1 := int64(r_OUT3 >> Bin_size3(r1))
	h2 := int64(r_OUT3 >> Bin_size3(r2))
	return Hash(interleave3(h2)<<2 | interleave3(h1)<<1 | interleave3(h0))
}

const (
	diam4  = 0xfffff
	r_IN4  = diam4 / 2
	r_OUT4 = r_IN4 + 1
)

func interleave4(x int64) uint64 {
	if x > diam4 {
		x = diam4
	}

	if x < 0 {
		x = 0
	}

	x = (x | x<<32) & 0xf800000007ff
	x = (x | x<<16) & 0xf80007c0003f
	x = (x | x<<8) & 0xc0380700c03807
	x = (x | x<<4) & 0x843084308430843
	x = (x | x<<2) & 0x909090909090909
	x = (x | x<<1) & 0x1111111111111111

	return uint64(x)
}

func Hash4(x0, x1, x2, x3 float32) Hash {
	h0 := int64(r_OUT4 * (x0 + 1))
	h1 := int64(r_OUT4 * (x1 + 1))
	h2 := int64(r_OUT4 * (x2 + 1))
	h3 := int64(r_OUT4 * (x3 + 1))
	return Hash(interleave4(h3)<<3 | interleave4(h2)<<2 | interleave4(h1)<<1 | interleave4(h0))
}

func Bin_size4(r float32) (sz uint8) {
	h := int64(r_OUT4 * r)
	if h > r_IN4 {
		h = r_IN4
	}
	if h < 0 {
		h = 0
	}

	for h != 0 {
		sz++
		h >>= 1
	}
	return sz
}

func Mask4(r float32) Hash {
	var mask Hash = math.MaxUint64
	return mask << 3 * Hash(Bin_size4(r))
}

func MaskRange4(r0, r1, r2, r3 float32) Hash {
	h0 := int64(r_OUT4 >> Bin_size4(r0))
	h1 := int64(r_OUT4 >> Bin_size4(r1))
	h2 := int64(r_OUT4 >> Bin_size4(r2))
	h3 := int64(r_OUT4 >> Bin_size4(r3))
	return Hash(interleave4(h3)<<3 | interleave4(h2)<<2 | interleave4(h1)<<1 | interleave4(h0))
}
