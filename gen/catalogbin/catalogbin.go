package main

import (
	"bufio"
	"encoding/binary"
	"errors"
	"flag"
	"fmt"
	"io"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/spenczar/fpc"
)

var force = flag.Bool("force", false, "always re-download the database")

func main() {
	flag.Parse()

	_, err := os.Stat(flag.Arg(0))
	if *force || os.IsNotExist(err) {
		resp, err := http.Get("https://github.com/UBNanosatLab/openstartracker/raw/91bcdea53ca222d8cc487eb4d512252ef0304321/hip_main.dat")
		if err != nil {
			panic(err)
		}
		f, err := os.Create(flag.Arg(0))
		if err != nil {
			panic(err)
		}
		_, err = io.Copy(f, resp.Body)
		if err != nil {
			panic(err)
		}
		f.Close()
	}

	f, err := os.Open(flag.Arg(0))
	if err != nil {
		panic(err)
	}

	fds := openfiles()

	rd := bufio.NewReader(f)

	count := 0
	for {
		line, err := rd.ReadString('\n')
		if !errors.Is(err, io.EOF) && err != nil {
			panic(err)
		}
		if line == "" && errors.Is(err, io.EOF) {
			break
		}
		hip_record := strings.Split(line, "|")
		if len(hip_record) != 78 {
			str := ""
			if err != nil {
				str = err.Error()
			}
			println(count)
			panic("invalid num fields, err: " + str)
		}
		fds.processRow(hip_record)
		if count < 10 {
			fmt.Println(
				hip_record[1],
				hip_record[5],
				hip_record[8],
				hip_record[12],
				hip_record[9],
				hip_record[13],
				hip_record[29],
				hip_record[6],
			)
		}
		count++
	}

	fds.closefiles()

	verifyFiles(count)
}

func (f *files) processRow(r []string) {
	var buf [100]byte

	for i := range r {
		r[i] = strings.Trim(r[i], " ")
	}

	idx, _ := strconv.ParseUint(r[1], 10, 64)
	n := binary.PutUvarint(buf[:], idx)
	mustWrite(f.idx, buf[:n])

	mag, _ := strconv.ParseFloat(r[5], 64)
	binary.LittleEndian.PutUint64(buf[:], math.Float64bits(mag))
	mustWrite(f.mag, buf[:8])

	ra, _ := strconv.ParseFloat(r[8], 64)
	binary.LittleEndian.PutUint64(buf[:], math.Float64bits(ra))
	mustWrite(f.ra_epoch, buf[:8])

	ra_rate, _ := strconv.ParseFloat(r[12], 64)
	binary.LittleEndian.PutUint64(buf[:], math.Float64bits(ra_rate))
	mustWrite(f.ra_rate, buf[:8])

	dec, _ := strconv.ParseFloat(r[9], 64)
	binary.LittleEndian.PutUint64(buf[:], math.Float64bits(dec))
	mustWrite(f.dec_epoch, buf[:8])

	dec_rate, _ := strconv.ParseFloat(r[13], 64)
	binary.LittleEndian.PutUint64(buf[:], math.Float64bits(dec_rate))
	mustWrite(f.dec_rate, buf[:8])

	rel1, _ := strconv.ParseUint(r[29], 10, 64)
	n = binary.PutUvarint(buf[:], rel1)
	mustWrite(f.reliable, buf[:n])

	rel2, _ := strconv.ParseUint(r[6], 10, 64)
	n = binary.PutUvarint(buf[:], rel2)
	mustWrite(f.reliable2, buf[:n])
}

type files struct {
	idx,
	mag,
	ra_epoch, ra_rate,
	dec_epoch, dec_rate,
	reliable, reliable2 io.WriteCloser
	mag_base, ra_epoch_base, ra_rate_base, dec_epoch_base, dec_rate_base io.Closer
}

func openfiles() *files {
	idx := mustCreate("data/index.var")
	mag_base := mustCreate("data/mag.fpc")
	ra_epoch_base := mustCreate("data/ra_epoch.fpc")
	ra_rate_base := mustCreate("data/ra_rate.fpc")
	dec_epoch_base := mustCreate("data/dec_epoch.fpc")
	dec_rate_base := mustCreate("data/dec_rate.fpc")
	reliable := mustCreate("data/rel1.var")
	reliable2 := mustCreate("data/rel2.var")

	mag := fpc.NewWriter(mag_base)
	ra_epoch := fpc.NewWriter(ra_epoch_base)
	ra_rate := fpc.NewWriter(ra_rate_base)
	dec_epoch := fpc.NewWriter(dec_epoch_base)
	dec_rate := fpc.NewWriter(dec_rate_base)

	return &files{
		idx:            idx,
		mag:            mag,
		ra_epoch:       ra_epoch,
		ra_rate:        ra_rate,
		dec_epoch:      dec_epoch,
		dec_rate:       dec_rate,
		reliable:       reliable,
		reliable2:      reliable2,
		mag_base:       mag_base,
		ra_epoch_base:  ra_epoch_base,
		ra_rate_base:   ra_rate_base,
		dec_epoch_base: dec_epoch_base,
		dec_rate_base:  dec_rate_base,
	}
}

func (f *files) closefiles() {
	f.idx.Close()
	f.mag.Close()
	f.ra_epoch.Close()
	f.ra_rate.Close()
	f.dec_epoch.Close()
	f.dec_rate.Close()
	f.reliable.Close()
	f.reliable2.Close()

	f.mag_base.Close()
	f.ra_epoch_base.Close()
	f.ra_rate_base.Close()
	f.dec_epoch_base.Close()
	f.dec_rate_base.Close()
}

func mustCreate(path string) *os.File {
	f, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	return f
}
func mustOpen(path string) *os.File {
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	return f
}

func mustWrite(w io.Writer, buf []byte) {
	n, err := w.Write(buf)
	if err != nil {
		panic(err)
	}
	if n != len(buf) {
		panic("short write")
	}
}

func decodevarint(dest *[]int, rd io.ByteReader) error {
	for {
		val, err := binary.ReadUvarint(rd)
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			return err
		}
		*dest = append(*dest, int(val))
	}
	return nil
}
func decodefpc(dest *[]float64, rd io.Reader) error {
	frd := fpc.NewReader(rd)
	count := 0
	for {
		val, err := frd.ReadFloat()
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			return fmt.Errorf("%w, count:%d", err, count)
		}
		*dest = append(*dest, val)
		count++
	}
	return nil
}

func verifyFiles(count int) {
	idx_base := mustOpen("data/index.var")
	defer idx_base.Close()
	mag_base := mustOpen("data/mag.fpc")
	defer mag_base.Close()
	ra_epoch_base := mustOpen("data/ra_epoch.fpc")
	defer ra_epoch_base.Close()
	ra_rate_base := mustOpen("data/ra_rate.fpc")
	defer ra_rate_base.Close()
	dec_epoch_base := mustOpen("data/dec_epoch.fpc")
	defer dec_epoch_base.Close()
	dec_rate_base := mustOpen("data/dec_rate.fpc")
	defer dec_rate_base.Close()
	reliable_base := mustOpen("data/rel1.var")
	defer reliable_base.Close()
	reliable2_base := mustOpen("data/rel2.var")
	defer reliable2_base.Close()

	idx := bufio.NewReader(idx_base)
	reliable := bufio.NewReader(reliable_base)
	reliable2 := bufio.NewReader(reliable2_base)

	var err error
	var fpc []float64
	var vint []int

	vint = make([]int, 0)
	err = decodevarint(&vint, idx)
	if err != nil || len(vint) != count {
		panic("decode error: " + fmt.Sprint(count, len(vint), err))
	}
	vint = make([]int, 0)
	err = decodevarint(&vint, reliable)
	if err != nil || len(vint) != count {
		panic("decode error: " + fmt.Sprint(count, len(vint), err))
	}
	vint = make([]int, 0)
	err = decodevarint(&vint, reliable2)
	if err != nil || len(vint) != count {
		panic("decode error: " + fmt.Sprint(count, len(vint), err))
	}
	fpc = make([]float64, 0)
	err = decodefpc(&fpc, mag_base)
	if err != nil || len(fpc) != count {
		panic("decode error: " + err.Error() + fmt.Sprint(count, len(fpc)))
	}
	fpc = make([]float64, 0)
	err = decodefpc(&fpc, ra_epoch_base)
	if err != nil || len(fpc) != count {
		panic("decode error: " + err.Error() + fmt.Sprint(count, len(fpc)))
	}
	fpc = make([]float64, 0)
	err = decodefpc(&fpc, ra_rate_base)
	if err != nil || len(fpc) != count {
		panic("decode error: " + err.Error() + fmt.Sprint(count, len(fpc)))
	}
	fpc = make([]float64, 0)
	err = decodefpc(&fpc, dec_epoch_base)
	if err != nil || len(fpc) != count {
		panic("decode error: " + err.Error() + fmt.Sprint(count, len(fpc)))
	}
	fpc = make([]float64, 0)
	err = decodefpc(&fpc, dec_rate_base)
	if err != nil || len(fpc) != count {
		panic("decode error: " + err.Error() + fmt.Sprint(count, len(fpc)))
	}
}
