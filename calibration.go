package openstartracker

import "math"

type Calibration struct {
	DBRedundancy  int
	DoubleStarPx  float32 // pixels between stars
	MaxFalseStars int
	PosErrSigma   float32
	RequiredStars int
	ThreshFactor  int

	BaseFlux      float32
	ImageHeight   int
	ImageVariance float32 // average dark frame value
	ImageWidth    int
	PixelScale    float32
	PosVariance   float32

	// derived
	KDBucketSize int
	MatchValue   float32
	MaxFov       float32
	MinFOV       float32
	PixXTangent  float32
	PixYTangent  float32
}

var CalDefaults = Calibration{
	DBRedundancy:  1,
	DoubleStarPx:  3.5,
	MaxFalseStars: 2,
	PosErrSigma:   2,
	RequiredStars: 5,
	ThreshFactor:  5,
}

// Compute computes the values of derived configuration values
func (c *Calibration) Compute() {
	c.MatchValue = float32(4*math.Log(1/(float64(c.ImageWidth)*float64(c.ImageHeight))) + math.Log(2*math.Pi))
	c.MinFOV = c.PixelScale * float32(c.ImageHeight)
	c.MaxFov = c.PixelScale * float32(math.Sqrt(float64(c.ImageWidth)*float64(c.ImageWidth)+float64(c.ImageHeight)*float64(c.ImageHeight)))
	c.PixXTangent = float32(2 * math.Tan((float64(c.ImageWidth)*float64(c.PixelScale)/3600)*math.Pi/(180*2)) / float64(c.ImageWidth))
	c.PixYTangent = float32(2 * math.Tan((float64(c.ImageHeight)*float64(c.PixelScale)/3600)*math.Pi/(180*2)) / float64(c.ImageHeight))
	c.KDBucketSize = int((float32(c.ImageWidth) * c.PixelScale / 3600) * (float32(c.ImageHeight) * c.PixelScale / 3600) * 3.5)
}
