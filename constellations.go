package openstartracker

import (
	"fmt"
	"sort"
)

type Constellation struct {
	P      float32
	S1, S2 int
	Idx    int
}

func (c *Constellation) String() string {
	return fmt.Sprintf(
		"constellation: p: %f, s1: %d, s2: %d, idx: %d",
		c.P,
		c.S1, c.S2,
		c.Idx,
	)
}

type Constellation_pair struct {
	Totalscore     float32
	Db_s1, Db_s2   int
	Img_s1, Img_s2 int
}

func (c *Constellation_pair) Flip() {
	c.Img_s1, c.Img_s2 = c.Img_s2, c.Img_s1
}

func (c *Constellation_pair) String() string {
	return fmt.Sprintf(
		"constellation_pair: totalscore: %f, db_s1: %d, db_s2: %d, img_s1: %d, img_s2: %d",
		c.Totalscore,
		c.Db_s1, c.Db_s2,
		c.Img_s1, c.Img_s2,
	)
}

func constellation_lt(c1, c2 *Constellation) bool {
	if c1.P != c2.P {
		return c1.P < c2.P
	} else if c1.S1 != c2.S1 {
		return c1.S1 < c2.S1
	}
	return c1.S2 < c2.S2
}

func constellation_lt_s1(c1, c2 *Constellation) bool { return c1.S1 < c2.S1 }
func constellation_lt_s2(c1, c2 *Constellation) bool { return c1.S2 < c2.S2 }
func constellation_lt_p(c1, c2 *Constellation) bool  { return c1.P < c2.P }

type Constellation_db struct {
	cfg     *Calibration
	Stars   *Star_db
	Results *Star_query
	Map     []*Constellation
}

func NewConstellation_db(cfg *Calibration, s *Star_db, stars_per_fov int, from_image bool) *Constellation_db {
	db := &Constellation_db{
		cfg:   cfg,
		Stars: s.Copy(),
	}
	db.Results = NewStar_query(cfg, db.Stars)
	if from_image {
		db.Results.Sort()
		ns := int(db.Stars.sz)
		if ns > stars_per_fov {
			ns = stars_per_fov
		}
		db.Map = make([]*Constellation, ns*(ns-1)/2)

		idx := 0
		for j := 1; j < ns; j++ {
			for i := 0; i < j; i++ {
				db.Map[idx] = &Constellation{
					P:  db.Results.Map[i].Dist_arcsec(*db.Results.Map[j]),
					S1: db.Results.Map[i].Star_idx,
					S2: db.Results.Map[j].Star_idx,
				}
				idx++
			}
		}
		sort.Slice(db.Map, func(i, j int) bool {
			return constellation_lt_p(db.Map[i], db.Map[j])
		})
		idx--
		for idx >= 0 {
			db.Map[idx].Idx = idx
			idx--
		}
	} else {
		db.Results.Kdmask_uniform_density(stars_per_fov)
		c_set := constellationSet{}
		for i := 0; i < len(db.Results.Map); i++ {
			if db.Results.Get_kdmask(i) == 0 {
				db.Results.Kdsearch(db.Results.Map[i].X, db.Results.Map[i].Y, db.Results.Map[i].Z, db.cfg.MaxFov, float32(db.cfg.ThreshFactor)*db.cfg.ImageVariance)
				r := db.Results.Results()
				for j := 0; j < len(r); j++ {
					if i != r[j] && db.Results.Map[i].Flux >= db.Results.Map[r[j]].Flux {
						c_set.insert(&Constellation{
							P:  db.Results.Map[i].Dist_arcsec(*db.Results.Map[r[j]]),
							S1: db.Results.Map[i].Star_idx,
							S2: db.Results.Map[r[j]].Star_idx,
						})
					}
				}
				db.Results.Clear_kdresults()
			}
		}
		db.Results.Reset_kdmask()
		db.Map = c_set
		for i, v := range db.Map {
			v.Idx = i
		}
	}

	return db
}

func (db *Constellation_db) String() string {
	return fmt.Sprintf(
		"constellation_db: stars: {%v}, results: {%v}",
		db.Stars,
		db.Results,
	)
}

type constellationSet []*Constellation

func (s *constellationSet) insert(c *Constellation) {
	i := sort.Search(len(*s), func(i int) bool {
		return !constellation_lt((*s)[i], c)
	})

	if i < len(*s) && *((*s)[i]) == *c {
		return
	}

	if i == len(*s) {
		*s = append(*s, c)
		return
	}

	*s = append((*s)[:i+1], (*s)[i:]...)
	(*s)[i] = c
}
