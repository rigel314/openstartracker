package openstartracker

import (
	"math"
)

type Centroid struct {
	X, Y, Mag float32
}

func Star_id(cfg *Calibration, centroids []Centroid, db *Constellation_db, results *Star_query) (ids []int) {
	ids = make([]int, len(centroids))
	for i := range ids {
		ids[i] = -1
	}
	img_stars := NewStar_db(cfg)
	for _, v := range centroids {
		s := NewImageStar(
			cfg,
			v.X-float32(cfg.ImageWidth)/2,
			-v.Y-float32(cfg.ImageHeight)/2,
			cfg.BaseFlux*(float32(math.Pow(10, float64(-v.Mag/2.5)))),
			-1,
		)
		img_stars.Append(s)
	}

	img_stars_n_brightest := img_stars.Copy_n_brightest(cfg.MaxFalseStars + cfg.RequiredStars)
	img_n_brightest := NewConstellation_db(cfg, img_stars_n_brightest, cfg.MaxFalseStars+2, true)
	lis := NewDb_match(cfg, db, img_n_brightest)

	if lis.P_match > .9 {
		x := lis.Winner.R11
		y := lis.Winner.R21
		z := lis.Winner.R31

		results.Kdsearch(x, y, z, cfg.MaxFov/2, float32(cfg.ThreshFactor)*cfg.ImageVariance)
		db.Results.Kdsearch(x, y, z, cfg.MaxFov/2, float32(cfg.ThreshFactor)*cfg.ImageVariance)
		near_stars := results.From_kdresults()
		fov_db := NewConstellation_db(cfg, near_stars, len(db.Results.Results()), true)
		db.Results.Clear_kdresults()
		results.Clear_kdresults()

		img := NewConstellation_db(cfg, img_stars, cfg.MaxFalseStars+2, true)
		fov_match := NewDb_match(cfg, fov_db, img)
		db_stars := fov_match.Winner.From_match()
		for i := 0; i < len(centroids); i++ {
			ids[i] = db_stars.Get_star(i).Id
		}
	}

	return
}
